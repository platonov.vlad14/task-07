package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
	private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
	private static final Properties properties = new Properties();
	private static Connection connection = null;
	private final String SQL_INSERT_USER = "INSERT INTO users VALUES (DEFAULT, ?)";
	private final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES (DEFAULT, ?)";
	private final String SQL_GET_USER = "SELECT * FROM users WHERE login = ?";
	private final String SQL_GET_TEAM = "SELECT*FROM teams WHERE name = ?";
	private final String SQL_FIND_ALL_USERS="SELECT * FROM users ORDER BY id";
	private final String SQL_FIND_ALL_TEAMS="SELECT * FROM teams ORDER BY id";
	private final String SQL_SET_TEAMS_FOR_USERS="INSERT INTO users_teams VALUES(?,?)";
	private final String SQL_GET_USER_TEAMS="SELECT teams.name,team_id FROM users_teams LEFT JOIN teams ON users_teams.team_id = teams.id AND user_id = ? WHERE teams.name IS NOT NULL";
	private final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE teams.name = ?";
	private final String SQL_DELETE_USER = "DELETE FROM users WHERE users.login = ?";
	private final String SQL_UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
	static {
		try {
			properties.load(new FileReader(String.valueOf(Paths.get("app.properties"))));
			connection = DriverManager.getConnection(properties.getProperty("connection.url"));
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
	}

	private static DBManager instance = new DBManager();

	public static synchronized DBManager getInstance() {
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_USERS)){
			final ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt("id"));
				user.setLogin(resultSet.getString("login"));
				users.add(user);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_USER)) {
			preparedStatement.setString(1,user.getLogin());
			preparedStatement.executeUpdate();
			user.setId(getUser(user.getLogin()).getId());
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_USER)) {
			for (User user: users) {
				preparedStatement.setString(1,user.getLogin());
				preparedStatement.executeUpdate();
			}
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}

		return false;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_USER)){
			preparedStatement.setString(1,login);
			final ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			user.setId(resultSet.getInt("id"));
			user.setLogin(resultSet.getString("login"));
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_TEAM)) {
			preparedStatement.setString(1,name);
			final ResultSet resultSet = preparedStatement.executeQuery();
			resultSet.next();
			team.setId(resultSet.getInt("id"));
			team.setName(resultSet.getString("name"));
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return team;

	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_ALL_TEAMS)){
			final ResultSet resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt("id"));
				team.setName(resultSet.getString("name"));
				teams.add(team);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_TEAM)) {
			preparedStatement.setString(1,team.getName());
			preparedStatement.executeUpdate();
			team.setId(getTeam(team.getName()).getId());
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_TEAMS_FOR_USERS)) {
			connection.setAutoCommit(false);
			if(getUser(user.getLogin())==null)
				insertUser(user);
			for (Team team: teams) {
				if(getTeam(team.getName())==null)
				insertTeam(team);
			}
			for (Team team : teams) {
					preparedStatement.setInt(1, getUser(user.getLogin()).getId());
					preparedStatement.setInt(2, getTeam(team.getName()).getId());
					preparedStatement.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
			return true;
		} catch (SQLException throwables) {
			if (connection != null) {
				try {
					connection.rollback();
					throw new DBException("Transaction fail",throwables);
				} catch (SQLException excep) {
					excep.printStackTrace();
				}
			}
		}
		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_USER_TEAMS)) {
			preparedStatement.setInt(1,getUser(user.getLogin()).getId());
			final ResultSet resultSet = preparedStatement.executeQuery();
			while(resultSet.next())
			{
				Team team = new Team();
				team.setId(resultSet.getInt(2));
				team.setName(resultSet.getString(1));
				teams.add(team);
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE_TEAM)) {
			preparedStatement.setString(1,team.getName());
			preparedStatement.executeUpdate();
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_TEAM)) {
			preparedStatement.setString(1,team.getName());
			preparedStatement.setInt(2,team.getId());
			preparedStatement.executeUpdate();
			return true;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return false;
	}

}
